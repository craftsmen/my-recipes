# My Recipes

## Getting started

NOTE: you need to run the project with Java 21, but you can also safely change the Java-version in the root `pom.xml` (I also tested it with Java 17 and that worked fine).

- First start the Docker container with `docker-compose up -d`
- Use IntelliJ to start `Application`. Or run it with Maven: `mvn -pl my-recipes-app spring-boot:run`.
- The backend will now run on `http://localhost:8080` (feel free to inspect the Swagger docs)
- In order to run the frontend, navigate to `./my-recipes-ui` and run `npm install` (to install the packages) and then `npm start`.

## Review the setup
- Inspect [proxy.conf.json](my-recipes-ui%2Fsrc%2Fproxy.conf.json), what does it do and why?
- Inspect [main.ts](my-recipes-ui%2Fsrc%2Fmain.ts) and [app.component.ts](my-recipes-ui%2Fsrc%2Fapp%2Fapp.component.ts). Do you notice the difference? (where are the modules?)

## Exercises

### 1. Show recipes

First, try to show the recipes on the screen:

- Implement a RecipeService that connects with the backend (e.g. using `HttpClient`)
- Note that the `GET /api/recipes` endpoint returns a paginated response. There is a `Page` TypeScript interface you can use, but for the rest of the models I refer you to the Swagger docs and figure it out yourself ;)
- Every endpoint has something nasty. It has some delay and might even sometimes throw a HTTP error. The challenge is to also handle those situations properly. If you struggle with this, you could disable this behavior by adjusting the code in the `RecipeController` and focus on the rest of the exercises.
- For this first exercise, you don't have to get the pagination to work, but if you want do to it right-away, you can continue with exercise 4.

### 2. Add a recipe

The test is simple: implement the form (either reactive or template-driven) and make sure it adds the recipe. 

Again: the loading states and the error states can make it a bit more challenging. But as a good Angular developer, you should know how to deal with this :)

### 3. Edit a recipe

Similar to adding a recipe. You should reuse the form component, if possible.

### 4. Pagination

For the pagination, I suggest the following requirements :)

- Implement the existing pagination component with the correct inputs/signals/whatever. Tip: if you pass it the current page number and the total number of pages, it should be enough to do its job.
- When you navigate to a different page, it should perform another request to the backend with the `page` query parameter (which is one-based).
- Extra kuddos if you store the page number in the URL state of the route (e.g. `?page=2`) so that when you reload your browser, it maintains the state. :) 
