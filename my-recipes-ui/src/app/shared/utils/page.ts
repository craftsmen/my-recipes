/**
 * You can use this utility to consume a paginated response from the backend.
 */
export interface Page<T> {
  firstPage: boolean;
  lastPage: boolean;
  pageNumber: number;
  pageSize: number;
  totalItemCount: number;
  totalPageCount: number;
  contents: T[];
}
