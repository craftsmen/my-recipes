import { catchError, concat, map, Observable, of, OperatorFunction } from 'rxjs';

/**
 * Just some utilities that you CAN use. If you need it ;)
 * Feel free to ignore.
 */

export type Loadable<T, E = unknown> = Loading | Failed<E> | Success<T>;

export interface Loading {
  readonly loading: true;
  readonly failed: false;
  readonly success: false;
}

export interface Failed<E> {
  readonly loading: false;
  readonly failed: true;
  readonly success: false;
  readonly error: E;
}

export interface Success<T> {
  readonly loading: false;
  readonly failed: false;
  readonly success: true;
  readonly result: T;
}

const LOADING: Loading = { loading: true, failed: false, success: false };

export function loading(): Loading {
  return LOADING;
}

export function success<T>(value: T): Success<T> {
  return { loading: false, failed: false, success: true, result: value };
}

export function failed<E>(error: E): Failed<E> {
  return { loading: false, failed: true, success: false, error };
}

export function asLoadable<T, E = any>(): OperatorFunction<T, Loadable<T, E>> {
  return (source$: Observable<T>) => concat(
    of<Loadable<T, E>>(LOADING),
    source$.pipe(
      map((value) => success(value)),
      catchError((error: E) => of(failed(error))),
    ),
  );
}
