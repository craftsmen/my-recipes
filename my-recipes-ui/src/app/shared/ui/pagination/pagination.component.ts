import { Component } from '@angular/core';

@Component({
  selector: 'app-pagination',
  standalone: true,
  template: `
    <ul class="pagination">
      <li class="page-item">
        <a class="page-link">Previous</a>
      </li>
      <!--<li class="page-item disabled">-->
      <!--  <span class="page-link">Previous</span>-->
      <!--</li>-->

      <li class="page-item">
        <a class="page-link">1</a>
      </li>
      <li class="page-item">
        <a class="page-link">2</a>
      </li>
      <li class="page-item">
        <a class="page-link active">3</a>
      </li>

      <!--<li class="page-item">-->
      <!--  <a class="page-link">Next</a>-->
      <!--</li>-->
      <li class="page-item disabled">
        <span class="page-link">Next</span>
      </li>
    </ul>

  `
})
export class PaginationComponent {
  // TODO: implement this component
}
