import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  template: `
    <div class="container my-5">
      <div class="p-5 text-center bg-body-tertiary rounded-3">
        <h1 class="text-body-emphasis">My Recipes</h1>
      </div>

      <div class="container my-5">
        <router-outlet/>
      </div>
    </div>
  `
})
export class AppComponent {}
