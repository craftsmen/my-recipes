import { Component } from '@angular/core';
import { RecipeFormComponent } from './recipe.form.component';

@Component({
  selector: 'app-recipe-edit',
  standalone: true,
  imports: [
    RecipeFormComponent
  ],
  template: `
    <h3 class="mb-3">Recept wijzigen</h3>

    <!-- this can be a nice indicator if you are still loading the recipe ;) -->
    <!--Bezig met ophalen recept...-->

    <!-- this can be a nice notification when something goes wrong ;) -->
    <!--<div class="alert alert-danger" role="alert">
      Er is een fout opgetreden tijdens het opslaan van het recept.
    </div>-->

    <app-recipe-form></app-recipe-form>
  `
})
export default class RecipeEditComponent {
  // TODO: implement this component
}
