import { Component } from '@angular/core';

import { PaginationComponent } from '../../../shared/ui/pagination/pagination.component';

@Component({
  selector: 'app-recipe-list',
  standalone: true,
  imports: [PaginationComponent],
  template: `

    <!-- this can be a nice way to visualize a loading state ;) -->
    <!--<div class="spinner-border" role="status">-->
    <!--  <span class="visually-hidden">Recipes worden geladen...</span>-->
    <!--</div>-->

    <!-- this can be a nice way to visualize that something went wrong ;) -->
    <!--<div class="alert alert-danger" role="alert">-->
    <!--  Er is een fout opgetreden tijdens het laden van de recepten.-->
    <!--  <a href="#">Probeer opnieuw.</a>-->
    <!--</div>-->

    <!-- this can be a nice way to visualize if there are no recipes yet ;) -->
    <!--<p class="lead">-->
    <!--  Er zijn nog geen recepten. <a href="#">Voeg er eentje toe!</a>-->
    <!--</p>-->

    <div class="mb-3">
      <button class="btn btn-primary">Nieuw recept toevoegen</button>
    </div>

    <app-pagination></app-pagination>

    <div class="list-group">
      <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">Biefstuk met truffel-parmezaanboter en gepofte rode ui</h5>
          <small>laatst aangepast: 2 dagen geleden</small>
        </div>
        <p class="mb-1">
          Een plakje truffel-parmezaanboter en een zoete gepofte ui maken de smaken van deze biefstuk nog lekkerder.
        </p>
      </a>
      <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">Ovenschotel met kabeljauw, witlof en mosterdroom</h5>
          <small>laatst aangepast: 3 dagen geleden</small>
        </div>
        <p class="mb-1">
          Een groot voordeel aan ovenschotels: snel gemaakt en superlekker! Deze variant maak je met minikrieltjes,
          witlof en kabeljauw. De zelfgemaakte mosterd-room zorgt voor een subtiele pittigheid.
        </p>
      </a>
    </div>
  `
})
export default class RecipeListComponent {
  // TODO: implement this component
}
