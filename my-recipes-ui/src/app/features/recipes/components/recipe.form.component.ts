import { Component } from '@angular/core';

@Component({
  selector: 'app-recipe-form',
  standalone: true,
  imports: [],
  template: `
    <div class="mb-3">
      <div class="form-floating">
        <input type="text" class="form-control form-control-lg" placeholder="Titel">
        <label>Titel</label>
      </div>
    </div>
    <div class="mb-3">
      <textarea class="form-control" placeholder="Inleiding" rows="2"></textarea>
    </div>
    <div class="mb-3 row">
      <div class="col-auto">
        <div class="input-group">
          <input type="number" class="form-control" placeholder="Bereidingstijd">
          <span class="input-group-text">min</span>
        </div>
      </div>
      <div class="col-auto">
        <div class="input-group">
          <input type="number" class="form-control" placeholder="Voedingwaarde">
          <span class="input-group-text">Kcal</span>
        </div>
      </div>
    </div>
    <div class="mb-3">
      <textarea class="form-control" placeholder="Instructies" rows="20"></textarea>
    </div>

    <!-- this can be a nice way to visualize a loading state... ;) -->
    <!-- <button class="btn btn-primary" type="button" disabled>
      <span class="spinner-border spinner-border-sm" aria-hidden="true"></span>
      <span class="visually-hidden" role="status">Bezig met opslaan...</span>
    </button>-->

    <button type="submit" class="btn btn-primary">
      Opslaan
    </button>

  `,
  styles: `
    :host { display: block }
  `
})
export class RecipeFormComponent {
  // TODO: implement this component
}


