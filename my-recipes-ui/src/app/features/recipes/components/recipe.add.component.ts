import { Component } from '@angular/core';
import { RecipeFormComponent } from './recipe.form.component';

@Component({
  selector: 'app-recipe-add',
  standalone: true,
  imports: [RecipeFormComponent],
  template: `
    <h3 class="mb-3">Nieuw recept toevoegen</h3>

    <!-- this can be a nice notification when something goes wrong ;) -->
    <!--<div class="alert alert-danger" role="alert">-->
    <!--  Er is een fout opgetreden tijdens het opslaan van het recept.-->
    <!--</div>-->

    <app-recipe-form></app-recipe-form>
  `
})
export default class RecipeAddComponent {
  // TODO: implement this component
}
