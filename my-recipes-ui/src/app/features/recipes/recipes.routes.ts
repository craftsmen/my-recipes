import { Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./recipes.feature.component'),
    children: [
      {
        path: '',
        loadComponent: () => import('./components/recipe.list.component'),
      },
      {
        path: 'add',
        loadComponent: () => import('./components/recipe.add.component'),
      },
      {
        path: 'edit/:id',
        loadComponent: () => import('./components/recipe.edit.component'),
      },
      // todo: feel free to also implement a detail page for the recipes!
    ]
  }
];

export default routes;
