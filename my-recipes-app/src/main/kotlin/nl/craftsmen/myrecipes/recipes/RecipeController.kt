package nl.craftsmen.myrecipes.recipes

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import nl.craftsmen.myrecipes.utils.ApiPage
import nl.craftsmen.myrecipes.utils.PaginationParameters
import nl.craftsmen.myrecipes.utils.Problem
import nl.craftsmen.myrecipes.utils.toApiPage
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus.I_AM_A_TEAPOT
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.time.OffsetDateTime
import java.util.*


@RestController
@RequestMapping("/api/recipes")
@Tag(name = "Recipes")
class RecipeController(
    private val repository: RecipeRepository
) {
    @GetMapping
    @PaginationParameters
    fun getRecipes(@Parameter(hidden = true) pageable: Pageable): ApiPage<ApiRecipe> = delayAndMaybeFailRandomly {
        repository.findAll(pageable)
            .map { it.toApi() }
            .toApiPage()
    }

    @GetMapping("/{id}")
    fun getRecipe(@PathVariable id: UUID): ApiRecipe = delayAndMaybeFailRandomly {
        repository.findByIdOrNull(id)?.toApi()
            ?: throw RecipeNotFoundProblem(id)
    }

    @PostMapping
    fun addRecipe(@RequestBody newRecipe: ApiNewRecipe): RecipeEntity = delayAndMaybeFailRandomly {
        repository.save(newRecipe.asNewEntity())
    }

    @PutMapping("/{id}")
    fun updateRecipe(@PathVariable id: UUID, @RequestBody update: ApiRecipe): RecipeEntity = delayAndMaybeFailRandomly {
        val existing = repository.findByIdOrNull(id) ?: throw RecipeNotFoundProblem(id)

        repository.save(existing.updatedWith(update))
    }

    @DeleteMapping("/{id}")
    fun deleteRecipe(@PathVariable id: UUID) = delayAndMaybeFailRandomly {
        val existing = repository.findByIdOrNull(id) ?: throw RecipeNotFoundProblem(id)

        repository.deleteById(existing.id)
    }

    private fun <R> delayAndMaybeFailRandomly(block: suspend () -> R): R = runBlocking {
        delay(1000) // to make loading indicators in the UI needed ;)
        if (Math.random() < 0.2) {
            throw RandomProblem("Please implement some error handling in the UI ;)")
        }
        block()
    }

    private fun ApiNewRecipe.asNewEntity() = RecipeEntity(
        id = UUID.randomUUID(),
        title = title,
        creationDateTime = OffsetDateTime.now(),
        lastModifiedDateTime = OffsetDateTime.now(),
        introduction = introduction,
        kiloCalories = kiloCalories,
        preparationTimeMinutes = preparationTimeMinutes,
        instructions = instructions,
        ingredients = emptyList(),
    )

    private fun RecipeEntity.updatedWith(update: ApiRecipe) = copy(
        title = update.title,
        lastModifiedDateTime = OffsetDateTime.now(),
        introduction = update.introduction,
        kiloCalories = update.kiloCalories,
        preparationTimeMinutes = update.preparationTimeMinutes,
        instructions = update.instructions
    )

    private fun RecipeEntity.toApi() = ApiRecipe(
        id = id,
        title = title,
        creationDateTime = creationDateTime,
        lastModifiedDateTime = lastModifiedDateTime,
        introduction = introduction,
        kiloCalories = kiloCalories,
        preparationTimeMinutes = preparationTimeMinutes,
        instructions = instructions
    )
}

data class RandomProblem(val reason: String) : Problem(
    status = I_AM_A_TEAPOT,
    detail = reason,
    type = URI.create("/problems/random-problem")
)

data class RecipeNotFoundProblem(val id: UUID) : Problem(
    status = NOT_FOUND,
    detail = "Recipe $id not found.",
    type = URI.create("/problems/not-found")
)

