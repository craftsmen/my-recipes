package nl.craftsmen.myrecipes.recipes

import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface RecipeRepository :
    CrudRepository<RecipeEntity, UUID>,
    PagingAndSortingRepository<RecipeEntity, UUID>
