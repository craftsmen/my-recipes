package nl.craftsmen.myrecipes.recipes

import jakarta.persistence.*
import java.time.OffsetDateTime
import java.util.*

@Entity
@Table(name = "recipes")
data class RecipeEntity(
    @Id val id: UUID,
    val title: String,
    val creationDateTime: OffsetDateTime,
    val lastModifiedDateTime: OffsetDateTime,
    val introduction: String,
    val kiloCalories: Long,
    val preparationTimeMinutes: Long,
    val instructions: String,
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="recipe_id")
    @OrderColumn(name="order")
    val ingredients: List<IngredientEntity> = emptyList()
)

@Entity
@Table(name = "ingredients")
data class IngredientEntity(
    @Id val id: UUID,
    val order: Int,
    val quantity: Long,
    val unit: String,
    val description: String
)
