package nl.craftsmen.myrecipes.recipes

import java.time.OffsetDateTime
import java.util.*

data class ApiRecipe(
    val id: UUID,
    val title: String,
    val creationDateTime: OffsetDateTime,
    val lastModifiedDateTime: OffsetDateTime,
    val introduction: String,
    val kiloCalories: Long,
    val preparationTimeMinutes: Long,
    val instructions: String
)

data class ApiNewRecipe(
    val title: String,
    val introduction: String,
    val kiloCalories: Long,
    val preparationTimeMinutes: Long,
    val instructions: String
)

