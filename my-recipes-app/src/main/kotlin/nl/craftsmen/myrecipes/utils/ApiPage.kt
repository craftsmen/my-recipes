package nl.craftsmen.myrecipes.utils

import org.springframework.data.domain.Page

/**
 * Simplification of @{org.springframework.data.domain.Page} to be used for the REST API.
 */
data class ApiPage<T>(
    val contents: List<T>,
    val firstPage: Boolean,
    val lastPage: Boolean,
    val pageNumber: Int,
    val pageSize: Int,
    val totalItemCount: Int,
    val totalPageCount: Int,
)

fun <T> Page<T>.toApiPage() = ApiPage<T>(
    contents = content,
    firstPage = isFirst,
    lastPage = isLast,
    pageSize = size,
    totalItemCount = totalElements.toInt(),
    totalPageCount = totalPages,

    // we configured the app to use one-based pagination,
    // but Spring Data still uses a zero-based page number in its Page object.
    pageNumber = number + 1,
)
