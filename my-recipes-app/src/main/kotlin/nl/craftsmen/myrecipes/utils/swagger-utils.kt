package nl.craftsmen.myrecipes.utils

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.Parameters
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Schema
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.*

@Target(FUNCTION, PROPERTY_GETTER, PROPERTY_SETTER)
@Retention(RUNTIME)
@Parameters(
    Parameter(
        `in` = ParameterIn.QUERY,
        name = "page",
        description = "The page number. The first page has page number 1.",
        schema = Schema(type = "integer", defaultValue = "1"),
        required = false
    ),
    Parameter(
        `in` = ParameterIn.QUERY,
        name = "page-size",
        description = "The maximum number of items per page. The maximum allowed page size is 100.",
        schema = Schema(type = "integer", defaultValue = "10"),
        required = false
    )
)
annotation class PaginationParameters
