package nl.craftsmen.myrecipes.utils

import org.springframework.http.HttpStatus
import org.springframework.http.ProblemDetail
import org.springframework.web.ErrorResponseException
import java.net.URI

abstract class Problem(
    status: HttpStatus,
    detail: String,
    type: URI
) : ErrorResponseException(
    status,
    ProblemDetail.forStatusAndDetail(status, detail).also { it.type = type },
    null
)
